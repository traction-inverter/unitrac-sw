################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
CC_SRCS := 
ASM_SRCS := 
C_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
CC_DEPS := 
C++_DEPS := 
OBJS := 
C_UPPER_DEPS := 
CXX_DEPS := 
SECONDARY_FLASH := 
SECONDARY_SIZE := 
ASM_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
modules/boot \
modules/energy_governor \
modules/logging \
modules/main_state_machine \
modules/pattern_modulator \
modules/signal_processing \
modules/system_diag \
modules/traction_command_interface \
system/FreeRTOS/CMSIS_RTOS_V2 \
system/FreeRTOS \
system/FreeRTOS/portable/GCC/ARM_CM4F \
system/FreeRTOS/portable/MemMang \
system/STM32G4xx_HAL_Driver \

