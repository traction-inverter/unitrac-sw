################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/FreeRTOS/cmsis_os2.c \
../modules/FreeRTOS/croutine.c \
../modules/FreeRTOS/event_groups.c \
../modules/FreeRTOS/heap_4.c \
../modules/FreeRTOS/list.c \
../modules/FreeRTOS/port.c \
../modules/FreeRTOS/queue.c \
../modules/FreeRTOS/stream_buffer.c \
../modules/FreeRTOS/tasks.c \
../modules/FreeRTOS/timers.c 

OBJS += \
./modules/FreeRTOS/cmsis_os2.o \
./modules/FreeRTOS/croutine.o \
./modules/FreeRTOS/event_groups.o \
./modules/FreeRTOS/heap_4.o \
./modules/FreeRTOS/list.o \
./modules/FreeRTOS/port.o \
./modules/FreeRTOS/queue.o \
./modules/FreeRTOS/stream_buffer.o \
./modules/FreeRTOS/tasks.o \
./modules/FreeRTOS/timers.o 

C_DEPS += \
./modules/FreeRTOS/cmsis_os2.d \
./modules/FreeRTOS/croutine.d \
./modules/FreeRTOS/event_groups.d \
./modules/FreeRTOS/heap_4.d \
./modules/FreeRTOS/list.d \
./modules/FreeRTOS/port.d \
./modules/FreeRTOS/queue.d \
./modules/FreeRTOS/stream_buffer.d \
./modules/FreeRTOS/tasks.d \
./modules/FreeRTOS/timers.d 


# Each subdirectory must supply rules for building sources it contributes
modules/FreeRTOS/%.o: ../modules/FreeRTOS/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/CMSIS/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/modules/FreeRTOS/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/STM32G4xx_HAL_Driver/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/modules/boot/inc" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


