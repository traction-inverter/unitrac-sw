################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/boot/app_freertos.c \
../modules/boot/main.c \
../modules/boot/stm32g4xx_hal_msp.c \
../modules/boot/stm32g4xx_hal_timebase_tim.c \
../modules/boot/stm32g4xx_it.c \
../modules/boot/system_stm32g4xx.c 

OBJS += \
./modules/boot/app_freertos.o \
./modules/boot/main.o \
./modules/boot/stm32g4xx_hal_msp.o \
./modules/boot/stm32g4xx_hal_timebase_tim.o \
./modules/boot/stm32g4xx_it.o \
./modules/boot/system_stm32g4xx.o 

C_DEPS += \
./modules/boot/app_freertos.d \
./modules/boot/main.d \
./modules/boot/stm32g4xx_hal_msp.d \
./modules/boot/stm32g4xx_hal_timebase_tim.d \
./modules/boot/stm32g4xx_it.d \
./modules/boot/system_stm32g4xx.d 


# Each subdirectory must supply rules for building sources it contributes
modules/boot/%.o: ../modules/boot/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/CMSIS/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/STM32G4xx_HAL_Driver/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/modules/boot/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/portable/GCC/ARM_CM4F" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/CMSIS_RTOS_V2" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


