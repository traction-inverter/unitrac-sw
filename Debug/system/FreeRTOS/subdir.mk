################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/FreeRTOS/croutine.c \
../system/FreeRTOS/event_groups.c \
../system/FreeRTOS/list.c \
../system/FreeRTOS/queue.c \
../system/FreeRTOS/stream_buffer.c \
../system/FreeRTOS/tasks.c \
../system/FreeRTOS/timers.c 

OBJS += \
./system/FreeRTOS/croutine.o \
./system/FreeRTOS/event_groups.o \
./system/FreeRTOS/list.o \
./system/FreeRTOS/queue.o \
./system/FreeRTOS/stream_buffer.o \
./system/FreeRTOS/tasks.o \
./system/FreeRTOS/timers.o 

C_DEPS += \
./system/FreeRTOS/croutine.d \
./system/FreeRTOS/event_groups.d \
./system/FreeRTOS/list.d \
./system/FreeRTOS/queue.d \
./system/FreeRTOS/stream_buffer.d \
./system/FreeRTOS/tasks.d \
./system/FreeRTOS/timers.d 


# Each subdirectory must supply rules for building sources it contributes
system/FreeRTOS/%.o: ../system/FreeRTOS/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/CMSIS/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/STM32G4xx_HAL_Driver/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/modules/boot/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/portable/GCC/ARM_CM4F" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/CMSIS_RTOS_V2" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


