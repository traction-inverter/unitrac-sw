################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/FreeRTOS/CMSIS_RTOS_V2/cmsis_os2.c 

OBJS += \
./system/FreeRTOS/CMSIS_RTOS_V2/cmsis_os2.o 

C_DEPS += \
./system/FreeRTOS/CMSIS_RTOS_V2/cmsis_os2.d 


# Each subdirectory must supply rules for building sources it contributes
system/FreeRTOS/CMSIS_RTOS_V2/%.o: ../system/FreeRTOS/CMSIS_RTOS_V2/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/CMSIS/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/STM32G4xx_HAL_Driver/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/modules/boot/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/portable/GCC/ARM_CM4F" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/CMSIS_RTOS_V2" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


