################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_adc.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_adc_ex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_cortex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_dma.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_dma_ex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_exti.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash_ex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash_ramfunc.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_gpio.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_pwr.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_pwr_ex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_rcc.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_rcc_ex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_tim.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_tim_ex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_uart.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_hal_uart_ex.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_ll_adc.c \
../system/STM32G4xx_HAL_Driver/stm32g4xx_ll_pwr.c 

OBJS += \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_adc.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_adc_ex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_cortex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_dma.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_dma_ex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_exti.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash_ex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash_ramfunc.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_gpio.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_pwr.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_pwr_ex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_rcc.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_rcc_ex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_tim.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_tim_ex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_uart.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_uart_ex.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_ll_adc.o \
./system/STM32G4xx_HAL_Driver/stm32g4xx_ll_pwr.o 

C_DEPS += \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_adc.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_adc_ex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_cortex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_dma.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_dma_ex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_exti.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash_ex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_flash_ramfunc.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_gpio.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_pwr.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_pwr_ex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_rcc.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_rcc_ex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_tim.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_tim_ex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_uart.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_hal_uart_ex.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_ll_adc.d \
./system/STM32G4xx_HAL_Driver/stm32g4xx_ll_pwr.d 


# Each subdirectory must supply rules for building sources it contributes
system/STM32G4xx_HAL_Driver/%.o: ../system/STM32G4xx_HAL_Driver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/CMSIS/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/STM32G4xx_HAL_Driver/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/modules/boot/inc" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/include" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/portable/GCC/ARM_CM4F" -I"/Users/cvorobev/eclipse-workspace/unitrac/system/FreeRTOS/CMSIS_RTOS_V2" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


