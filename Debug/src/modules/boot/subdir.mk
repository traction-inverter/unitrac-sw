################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/modules/boot/app_freertos.c \
../src/modules/boot/main.c \
../src/modules/boot/stm32g4xx_hal_msp.c \
../src/modules/boot/stm32g4xx_hal_timebase_tim.c \
../src/modules/boot/stm32g4xx_it.c \
../src/modules/boot/stm32g4xx_nucleo.c \
../src/modules/boot/system_stm32g4xx.c 

OBJS += \
./src/modules/boot/app_freertos.o \
./src/modules/boot/main.o \
./src/modules/boot/stm32g4xx_hal_msp.o \
./src/modules/boot/stm32g4xx_hal_timebase_tim.o \
./src/modules/boot/stm32g4xx_it.o \
./src/modules/boot/stm32g4xx_nucleo.o \
./src/modules/boot/system_stm32g4xx.o 

C_DEPS += \
./src/modules/boot/app_freertos.d \
./src/modules/boot/main.d \
./src/modules/boot/stm32g4xx_hal_msp.d \
./src/modules/boot/stm32g4xx_hal_timebase_tim.d \
./src/modules/boot/stm32g4xx_it.d \
./src/modules/boot/stm32g4xx_nucleo.d \
./src/modules/boot/system_stm32g4xx.d 


# Each subdirectory must supply rules for building sources it contributes
src/modules/boot/%.o: ../src/modules/boot/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32g4xx" -I"/Users/cvorobev/eclipse-workspace/unitrac/src/modules/FreeRTOS/include" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


