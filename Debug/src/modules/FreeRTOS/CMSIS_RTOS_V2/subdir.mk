################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/modules/FreeRTOS/CMSIS_RTOS_V2/cmsis_os2.c 

OBJS += \
./src/modules/FreeRTOS/CMSIS_RTOS_V2/cmsis_os2.o 

C_DEPS += \
./src/modules/FreeRTOS/CMSIS_RTOS_V2/cmsis_os2.d 


# Each subdirectory must supply rules for building sources it contributes
src/modules/FreeRTOS/CMSIS_RTOS_V2/%.o: ../src/modules/FreeRTOS/CMSIS_RTOS_V2/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32g4xx" -I"/Users/cvorobev/eclipse-workspace/unitrac/src/modules/FreeRTOS/include" -I"/Users/cvorobev/eclipse-workspace/unitrac/src/modules/boot/inc" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


