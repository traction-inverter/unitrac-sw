################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/modules/FreeRTOS/croutine.c \
../src/modules/FreeRTOS/event_groups.c \
../src/modules/FreeRTOS/heap_4.c \
../src/modules/FreeRTOS/list.c \
../src/modules/FreeRTOS/port.c \
../src/modules/FreeRTOS/queue.c \
../src/modules/FreeRTOS/stream_buffer.c \
../src/modules/FreeRTOS/tasks.c \
../src/modules/FreeRTOS/timers.c 

OBJS += \
./src/modules/FreeRTOS/croutine.o \
./src/modules/FreeRTOS/event_groups.o \
./src/modules/FreeRTOS/heap_4.o \
./src/modules/FreeRTOS/list.o \
./src/modules/FreeRTOS/port.o \
./src/modules/FreeRTOS/queue.o \
./src/modules/FreeRTOS/stream_buffer.o \
./src/modules/FreeRTOS/tasks.o \
./src/modules/FreeRTOS/timers.o 

C_DEPS += \
./src/modules/FreeRTOS/croutine.d \
./src/modules/FreeRTOS/event_groups.d \
./src/modules/FreeRTOS/heap_4.d \
./src/modules/FreeRTOS/list.d \
./src/modules/FreeRTOS/port.d \
./src/modules/FreeRTOS/queue.d \
./src/modules/FreeRTOS/stream_buffer.d \
./src/modules/FreeRTOS/tasks.d \
./src/modules/FreeRTOS/timers.d 


# Each subdirectory must supply rules for building sources it contributes
src/modules/FreeRTOS/%.o: ../src/modules/FreeRTOS/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32G474xx -DTRACE -DOS_USE_TRACE_ITM -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32g4xx" -I"/Users/cvorobev/eclipse-workspace/unitrac/src/modules/FreeRTOS/include" -I"/Users/cvorobev/eclipse-workspace/unitrac/src/modules/boot/inc" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


