/**
 * \file 		traction_command_interface.h
 * \author 		Christian Vorobev
 * \date 		26 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef MODULES_TRACTION_COMMAND_INTERFACE_INC_TRACTION_COMMAND_INTERFACE_H_
#define MODULES_TRACTION_COMMAND_INTERFACE_INC_TRACTION_COMMAND_INTERFACE_H_

typedef int tcmd_if_return_t;
#define TCMD_SUCCESS 0
#define TCMD_ERR_THREAD_START 1

tcmd_if_return_t traction_command_interface_init();
tcmd_if_return_t traction_command_interface_selftest();
tcmd_if_return_t traction_command_interface_start();

#endif /* MODULES_TRACTION_COMMAND_INTERFACE_INC_TRACTION_COMMAND_INTERFACE_H_ */
