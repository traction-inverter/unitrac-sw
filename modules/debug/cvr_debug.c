/**
 * \file 		cvr_debug.c
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "inc/cvr_debug.h"
#include "hal/cvr_debug_hal.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define CVR_DEBUG_BUFSIZE 512
#define _ISR_MAGICSTRING "\n" KGRN "[ISR]"

char _print_buffer[CVR_DEBUG_BUFSIZE]; /// Statically allocated buffer as stack space tends to be too expensive.

char* _prefix(int l){
	switch(l){
	case CVR_DEBUG_LEVEL_NOSUPPRESS:
		return "\n" KRED "[N] ";
	case CVR_DEBUG_LEVEL_ERROR:
		return "\n" KRED "[E] ";
	case CVR_DEBUG_LEVEL_WARNING:
		return "\n" KYEL "[W] ";
	case CVR_DEBUG_LEVEL_INFO:
		return "\n" KBLU "[I] ";
	case CVR_DEBUG_LEVEL_VERBOSE:
		return "\n" KWHT "[V] ";
	case CVR_DEBUG_LEVEL_DEBUG:
		return "\n" KWHT "[D] ";
	default:
		return "\n" KNRM "[?] ";
	}
}

void cvr_debug(int level, const char *fmt, ...){
	char *prefix = _prefix(level);
	va_list args;
	va_start(args, fmt);
	vsnprintf(_print_buffer, CVR_DEBUG_BUFSIZE, fmt, args);
	/// In case vsnprintf runs out of buffer space
	_print_buffer[CVR_DEBUG_BUFSIZE] = "\0";
	if(hal_cvr_debug_is_in_irq()){
		hal_cvr_write_buffer(_ISR_MAGICSTRING);
		hal_cvr_write_buffer(_print_buffer);
		hal_cvr_write_buffer("\n");
	} else {
		hal_cvr_debug_lock();
		hal_cvr_write_buffer(prefix);
		hal_cvr_write_buffer(_print_buffer);
		hal_cvr_debug_release();
	}
}
