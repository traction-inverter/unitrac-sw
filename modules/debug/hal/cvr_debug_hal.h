/**
 * \file 		cvr_debug_hal.h
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 */

#ifndef MODULES_DEBUG_HAL_CVR_DEBUG_HAL_H_
#define MODULES_DEBUG_HAL_CVR_DEBUG_HAL_H_

#include <stdint.h>

/**
 * Returns current system time. Use only for debug logging purposes as this number
 * is not overflow safe.
 * This function is assumed to be ISR-safe and non-blocking.
 * @return System Time (arbitrary time unit)
 */
uint32_t hal_cvr_debug_get_time();

/**
 * Writes buffer `buf` to logging interface.
 * The buffer shall be treated as NULL-terminated.
 * This function is assumed to be ISR-safe and non-blocking.
 */
void hal_cvr_write_buffer(const char *buf);

/**
 * Mutex for printing to prevent debug message overlapping. Lock function.
 * This function is not called inside of ISR.
 */
void hal_cvr_debug_lock();

/**
 * Mutex for printing to prevent debug message overlapping. Release function.
 * This function is not called inside of ISR.
 */
void hal_cvr_debug_release();

/**
 * Returns whether the current context is ISR.
 * This function is assumed to be ISR-safe and non-blocking.
 * @return 0 No IRQ context active.
 * @return nonzero IRQ context active.
 */
uint32_t hal_cvr_debug_is_in_irq();

#endif /* MODULES_DEBUG_HAL_CVR_DEBUG_HAL_H_ */
