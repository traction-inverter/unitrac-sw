/**
 * \file 		cvr_debug.c
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "../cvr_debug_hal.h"
#include "cmsis_os.h"
#include <stdint.h>

uint8_t is_started = 0;

osMutexId_t _print_mutex;

uint32_t hal_cvr_debug_get_time(){
	return osKernelGetTickCount();
}

void hal_cvr_write_buffer(const char *buf, size_t len){
	/// TODO: Insert inverter-specific DMA ringbuffer printing implementation.
}

void hal_cvr_debug_lock(){
	if(is_started != 1){
		is_started = 1;
		_print_mutex = osMutexNew(NULL);
		osMutexRelease(_print_mutex);
	}
	osMutexAcquire(_print_mutex, 0);

}

void hal_cvr_debug_release(){
	if(is_started != 1) return;
	osMutexRelease(_print_mutex);
}
