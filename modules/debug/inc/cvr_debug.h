/**
 * \file 		cvr_debug.h
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef MODULES_DEBUG_CVR_DEBUG_H_
#define MODULES_DEBUG_CVR_DEBUG_H_

typedef enum cvr_debug_level_t{
	CVR_DEBUG_LEVEL_NOSUPPRESS = 0,
	CVR_DEBUG_LEVEL_ERROR = 1,
	CVR_DEBUG_LEVEL_WARNING = 2,
	CVR_DEBUG_LEVEL_INFO = 3,
	CVR_DEBUG_LEVEL_VERBOSE = 4,
	CVR_DEBUG_LEVEL_DEBUG = 5,
};

void cvr_debug(int level, const char *fmt, ...);
#define CVR_ASSERT(X) if(!X){cvr_debug(CVR_DEBUG_LEVEL_ERROR, "Assertion failure: %s:%s, %s.", __FILE__, __LINE__, __FUNCTION__);\
	__asm__ __volatile__ ("bkpt #0");}

#define CVR_FENTRY() cvr_debug(CVR_DEBUG_LEVEL_DEBUG, "Fn Entry: %s.", __FUNCTION__);
#define CVR_FEXIT() cvr_debug(CVR_DEBUG_LEVEL_DEBUG, "Fn Exit: %s.", __FUNCTION__);

#endif /* MODULES_DEBUG_CVR_DEBUG_H_ */
