/**
 * \file 		logging.c
 * \author 		Christian Vorobev
 * \date 		26 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "inc/logging.h"
#include "cmsis_os.h"

osThreadId_t data_loggingHandle;
void task_data_logging(void *argument);

logging_return_t logging_start(){
	const osThreadAttr_t data_logging_attributes = { .name = "data_logging",
			.priority = (osPriority_t) osPriorityLow, .stack_size = 128 };
	data_loggingHandle = osThreadNew(task_data_logging, NULL,
			&data_logging_attributes);
	return LOGGING_SUCCESS;
}

/**
 * @brief Function implementing the data_logging thread.
 * @param argument: Not used
 * @retval None
 */
void task_data_logging(void *argument) {
	for (;;) {
		osDelay(1);
	}
}
