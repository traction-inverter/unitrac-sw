/**
 * \file 		logging.h
 * \author 		Christian Vorobev
 * \date 		26 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef MODULES_LOGGING_INC_LOGGING_H_
#define MODULES_LOGGING_INC_LOGGING_H_

typedef int logging_return_t;
#define LOGGING_SUCCESS 0
#define LOGGING_ERR_THREAD_START 1

logging_return_t logging_init();
logging_return_t logging_selftest();
logging_return_t logging_start();
#endif /* MODULES_LOGGING_INC_LOGGING_H_ */
