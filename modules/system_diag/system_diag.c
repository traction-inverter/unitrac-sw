/**
 * \file 		system_diag.c
 * \author 		Christian Vorobev
 * \date 		26 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "inc/system_diag.h"
#include "cmsis_os.h"

osThreadId_t system_diagHandle;
void task_system_diag(void *argument);

sdiag_return_t system_diag_start(){
	/* definition and creation of system_diag */
	const osThreadAttr_t system_diag_attributes = { .name = "system_diag",
			.priority = (osPriority_t) osPriorityNormal1, .stack_size = 128 };
	system_diagHandle = osThreadNew(task_system_diag, NULL,
			&system_diag_attributes);
	return SDIAG_SUCCESS;
}

void task_system_diag(void *argument) {
	for (;;) {
		osDelay(1);
	}
}
