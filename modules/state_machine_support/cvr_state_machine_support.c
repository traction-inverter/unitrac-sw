/**
 * \file 		cvr_state_machine_support.c
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "inc/cvr_state_machine_support.h"
#include "hal/cvr_state_machine_hal.h"
#include <stddef.h>

struct cvr_transition_descriptor{
	cvr_transition_fn transition_function;
	void *transition_fn_args;
};

struct cvr_state_machine{
	int evt_count;
	struct cvr_transition_descriptor *tdesc; /// pointer to function-pointer table which is allocated below.
	struct cvr_transition_descriptor d_fn;
	cvr_sm_init_function ifn;
	cvr_sm_thread_priority_t t_prio;
	cvr_sm_queue_handle event_q;
};

void _cvr_evt_default_event(void *args);

cvr_sm_return_t cvr_sm_new(cvr_sm_handle s, size_t number_of_events, cvr_sm_init_function init_function, void *args, cvr_sm_thread_priority_t sm_priority){
	if(number_of_events<1){
		return CVR_SM_ERR_ARG;
	}
	s = malloc(sizeof(struct cvr_state_machine));
	if(s == NULL ){
		/// Nothing to free here.
		return CVR_SM_ERR_CREATE;
	}
	s->evt_count = number_of_events;
	s->tdesc = malloc(number_of_events * sizeof(struct cvr_transition_descriptor));
	s->t_prio = sm_priority;
	if(s->tdesc == NULL){
		free(s);
		return CVR_SM_ERR_CREATE;
	}
	/// Create queue
	if(cvr_sm_hal_queue_new(s->event_q) != CVR_SM_HAL_OK){
		free(s->tdesc);
		free(s);
		return CVR_SM_ERR_CREATE;
	}
	/// Create thread
	if(cvr_sm_hal_thread_create(_cvr_sm_thread_function, (void *)s, s->t_prio) != CVR_SM_HAL_OK){
		free(s->tdesc);
		free(s);
		return CVR_SM_ERR_CREATE;
	}

	return CVR_SM_OK;
}

cvr_sm_return_t cvr_sm_default_fn_register(cvr_sm_handle s, cvr_transition_fn default_fn, void *f_args){
	if(s == NULL){
		return CVR_SM_ERR_ARG;
	}
	cvr_sm_return_t _r = CVR_SM_ERR;

	if (default_fn != NULL) {
		s->d_fn.transition_function = default_fn;
		s->d_fn.transition_fn_args = f_args;
	} else {
		s->d_fn.transition_function = _cvr_evt_default_event;
	}
	_r = CVR_SM_OK;

	return _r;
}

cvr_sm_return_t cvr_sm_transition_bind(cvr_sm_handle s, cvr_event_id_t e, cvr_transition_fn f){
	if(s == NULL) return CVR_SM_ERR_ARG;
	if((s->evt_count > e)) return CVR_SM_ERR_ARG;
	cvr_sm_return_t _r = CVR_SM_ERR;

	s->tdesc[e]->transition_function = f;
	_r = CVR_SM_OK;

	return _r;
}

cvr_sm_return_t cvr_sm_enqueue_event(cvr_sm_handle s, cvr_event_id_t e){
	if(s == NULL) return CVR_SM_ERR_ARG;
	if(s->evt_count > e) return CVR_SM_ERR_ARG;
	cvr_sm_return_t _r = CVR_SM_ERR;

	s->tdesc[e]->transition_function(s->tdesc[e]->transition_fn_args);
	_r = CVR_SM_OK;

	return _r;
}

void _cvr_sm_thread_function(void *args){
	cvr_sm_handle s = (cvr_sm_handle)args;
	/// Call init function
	s->ifn();
	cvr_event_id_t e;
	for(;;){
		cvr_sm_hal_queue_get(s->event_q, &e);
		struct cvr_transition_descriptor _to_execute;
		memcpy(_to_execute, s->tdesc[e], sizeof(struct cvr_transition_descriptor));
		_cvr_sm_transition_clear(s);
		_to_execute.transition_function(_to_execute.transition_fn_args);
	}
}

cvr_sm_return_t _cvr_sm_transition_clear(cvr_sm_handle s){
	if(s == NULL) return CVR_SM_ERR_ARG;
	cvr_sm_return_t _r = CVR_SM_ERR;

	for(int i=0;i<s->evt_count;i++){
		s->tdesc[i] = s->d_fn;
	}
	_r = CVR_SM_OK;

	return _r;
}

/**
 * State Machine Unsupported Event Function if user has not supplied an error handler
 * @param args
 */
void _cvr_evt_default_event(void *args){
	CVR_ASSERT(0);
}
