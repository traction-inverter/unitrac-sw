/**
 * \file 		cvr_state_machine_hal_impl_cmsis.c
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * \copyright {
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * }
 */

#include "../cvr_state_machine_hal.h"
#include "../../inc/cvr_state_machine_support.h"
#include "cmsis_os2.h"

struct cvr_sm_queue{
	osMessageQueueId_t q_id;
};

cvr_sm_hal_return_t cvr_sm_hal_queue_new(cvr_sm_queue_handle q){
	q->q_id = osMessageQueueNew(10, sizeof(cvr_event_id_t), NULL);
	cvr_sm_hal_return_t _r = CVR_SM_HAL_ERR;

	if(q->q_id != NULL) _r = CVR_SM_HAL_OK;

	return _r;
}

cvr_sm_hal_return_t cvr_sm_hal_queue_put(cvr_sm_queue_handle q, cvr_event_id_t *e){
	if(q == NULL || e == NULL) return CVR_SM_HAL_ERR;
	cvr_sm_hal_return_t _r = CVR_SM_HAL_ERR;

	if(osMessageQueuePut(q->q_id, e, 0, 0) == osOK) _r = CVR_SM_HAL_OK;

	return _r;
}

cvr_sm_hal_return_t cvr_sm_hal_queue_get(cvr_sm_queue_handle q, cvr_event_id_t *e){
	if(q == NULL || e == NULL) return CVR_SM_HAL_ERR;
	cvr_sm_hal_return_t _r = CVR_SM_HAL_ERR;

	if(osMessageQueueGet(q->q_id, e, NULL, osWaitForever) == osOK) _r = CVR_SM_HAL_OK;

	return _r;
}

cvr_sm_hal_return_t cvr_sm_hal_thread_create(target_function_t f, void* args, cvr_sm_thread_priority_t p){
	osThreadAttr_t thread_attr;
	thread_attr.stack_size = 128;
	switch(p){
	case CVR_THREAD_PRIO_IDLE:
		thread_attr.priority = osPriorityIdle;
		break;
	case CVR_THREAD_PRIO_LOW:
		thread_attr.priority = osPriorityLow;
		break;
	case CVR_THREAD_PRIO_BELOW_NORMAL:
		thread_attr.priority = osPriorityBelowNormal;
		break;
	case CVR_THREAD_PRIO_NORMAL:
		thread_attr.priority = osPriorityNormal;
		break;
	case CVR_THREAD_PRIO_ABOVE_NORMAL:
		thread_attr.priority = osPriorityAboveNormal;
		break;
	case CVR_THREAD_PRIO_BELOW_HIGH:
		thread_attr.priority = osPriorityAboveNormal1;
		break;
	case CVR_THREAD_PRIO_HIGH:
		thread_attr.priority = osPriorityHigh;
		break;
	case CVR_THREAD_PRIO_ABOVE_HIGH:
		thread_attr.priority = osPriorityHigh1;
		break;
	case CVR_THREAD_PRIO_BELOW_REALTIME:
		thread_attr.priority = osPriorityHigh2;
		break;
	case CVR_THREAD_PRIO_REALTIME:
		thread_attr.priority = osPriorityRealtime;
		break;
	case CVR_THREAD_PRIO_ISR:
		thread_attr.priority = osPriorityISR;
		break;
	default:
		return CVR_SM_HAL_ERR;
	}
	if(osThreadNew(f, args, &thread_attr) == osOK) return CVR_SM_HAL_OK;
	return CVR_SM_HAL_ERR;
}
