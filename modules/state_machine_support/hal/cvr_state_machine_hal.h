/**
 * \file 		cvr_state_machine_hal.h
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * \copyright {
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * }
 */

#ifndef MODULES_STATE_MACHINE_SUPPORT_HAL_CVR_STATE_MACHINE_HAL_H_
#define MODULES_STATE_MACHINE_SUPPORT_HAL_CVR_STATE_MACHINE_HAL_H_

#include "../inc/cvr_state_machine_support.h"
typedef enum{
	CVR_SM_HAL_OK,
	CVR_SM_HAL_ERR
}cvr_sm_hal_return_t;
typedef void (*target_function_t)(void *);
typedef struct cvr_sm_queue *cvr_sm_queue_handle;

/**
 * Creates a new queue.
 * @param q Uninitialised queue pointer.
 * @return CVR_SM_HAL_OK Queue has been created successfully
 * @return CVR_SM_HAL_ERR Queue could not be created.
 */
cvr_sm_hal_return_t cvr_sm_hal_queue_new(cvr_sm_queue_handle q);

/**
 * Puts an event into `e`.
 * @param q Queue handle of queue to put event `e` into
 * @param e Event to put into queue designated by `q`
 * @return CVR_SM_HAL_OK Event has been put into queue `q` successfully.
 * @return CVR_SM_HAL_ERR Failed to put event into queue `q`.
 */
cvr_sm_hal_return_t cvr_sm_hal_queue_put(cvr_sm_queue_handle q, cvr_event_id_t *e);

/**
 * Gets an event indicated by `e` from queue.
 * @param q Queue handle of queue to wait for a new event for.
 * @param e {Pointer to event to store event data of type `cvr_event_id_t` in.
 * Note that the user must copy event data into `e`.}
 * @return CVR_SM_HAL_OK Event data has been stored in variable pointed to by `e`.
 * @return CVR_SM_HAL_ERR Failed to get new event.
 */
cvr_sm_hal_return_t cvr_sm_hal_queue_get(cvr_sm_queue_handle q, cvr_event_id_t *e);

/**
 * Creates a new thread.
 * @param f Target execution function to execute upon thread creation.
 * @param args Argument to pass to target execution function.
 * @param p Priority to assign to thread.
 * @return CVR_SM_HAL_OK Thread has been created successfully and is running.
 * @return CVR_SM_HAL_ERR Failed to create thread.
 */
cvr_sm_hal_return_t cvr_sm_hal_thread_create(target_function_t f, void* args, cvr_sm_thread_priority_t p);

#endif /* MODULES_STATE_MACHINE_SUPPORT_HAL_CVR_STATE_MACHINE_HAL_H_ */
