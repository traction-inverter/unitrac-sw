/**
 * \file 		cvr_state_machine_support.h
 * \author 		Christian Vorobev
 * \date 		29 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef MODULES_STATE_MACHINE_SUPPORT_INC_CVR_STATE_MACHINE_SUPPORT_H_
#define MODULES_STATE_MACHINE_SUPPORT_INC_CVR_STATE_MACHINE_SUPPORT_H_

typedef struct cvr_state_machine* cvr_sm_handle;
typedef unsigned int cvr_event_id_t;
typedef void (*cvr_transition_fn)(void *args);
typedef void (*cvr_sm_init_function)(void *args);
typedef enum{
	CVR_SM_OK,
	CVR_SM_ERR_CREATE,
	CVR_SM_ERR_BIND,
	CVR_SM_ERR_ARG,
	CVR_SM_ERR_OVF,
	CVR_SM_ERR,
}cvr_sm_return_t;

typedef enum {
	CVR_THREAD_PRIO_IDLE,
	CVR_THREAD_PRIO_LOW,
	CVR_THREAD_PRIO_BELOW_NORMAL,
	CVR_THREAD_PRIO_NORMAL,
	CVR_THREAD_PRIO_ABOVE_NORMAL,
	CVR_THREAD_PRIO_BELOW_HIGH,
	CVR_THREAD_PRIO_HIGH,
	CVR_THREAD_PRIO_ABOVE_HIGH,
	CVR_THREAD_PRIO_BELOW_REALTIME,
	CVR_THREAD_PRIO_REALTIME,
	CVR_THREAD_PRIO_ISR
}cvr_sm_thread_priority_t;

/**
 * Creates a new state machine handle.
 * @param s State Machine handle to initialise
 * @param number_of_events Number of events to track in state machine
 * @param init_function Function firstly called when the state machine thread starts.
 * @param args Function arguments to call `init_function` with.
 * @return CVR_SM_ERR_ARG `number_of_events` is smaller than 1
 * @return CVR_SM_ERR_CREATE Failed to allocate memory
 * @return CVR_SM_OK State Machine created successfully
 */
cvr_sm_return_t cvr_sm_new(cvr_sm_handle s, size_t number_of_events, cvr_sm_init_function init_function, void *args, cvr_sm_thread_priority_t sm_priority);

/**
 * Registers a default transition handler `default_fn`.
 * @param s State Machine to which to associate the default handler `default_fn`
 * @param default_fn Default handler to register.
 * @param f_args Arguments to pass to default handler; may be NULL. Note that only a reference is stored in `s`.
 * @return CVR_SM_ERR_ARG `s` is not initialised or `default_fn` is invalid.
 * @return CVR_SM_OK Default handler registered successfully.
 */
cvr_sm_return_t cvr_sm_default_fn_register(cvr_sm_handle s, cvr_transition_fn default_fn, void *f_args);

/**
 * Binds a new transition to an event `e` in `s`.
 * @param s State Machine handle where event `e` shall be bound to `f`
 * @param e Event to which to bind `f`
 * @param f Function which shall be bound to `e`
 * @return CVR_SM_ERR_ARG `s` is not initialised or `e` is not within range of `number_of_events` defined in `cvr_sm_new`
 * @return CVR_SM_OK `f` bound to even `e` successfully
 */
cvr_sm_return_t cvr_sm_transition_bind(cvr_sm_handle s, cvr_event_id_t e, cvr_transition_fn f);

/**
 * Evaluates an event `e` for State Machine `s`
 * @param s State Machine handle on which to evaluate event `e`
 * @param e Event which shall be evaluated on State Machine `s`
 * @return CVR_SM_ERR_ARG `s` is not initialised or `e` is not within range of `number_of_events` defined in `cvr_sm_new`
 * @return CVR_SM_OVF Could not insert event due to the event queue being full.
 * @return CVR_SM_OK `e` evaluated successfully
 */
cvr_sm_return_t cvr_sm_enqueue_event(cvr_sm_handle s, cvr_event_id_t e);

#endif /* MODULES_STATE_MACHINE_SUPPORT_INC_CVR_STATE_MACHINE_SUPPORT_H_ */
