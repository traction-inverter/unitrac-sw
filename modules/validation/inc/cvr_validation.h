/**
 * \file 		cvr_validation.h
 * \author 		Christian Vorobev
 * \date 		1 Oct 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * \copyright {
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * }
 */

#ifndef MODULES_VALIDATION_INC_CVR_VALIDATION_H_
#define MODULES_VALIDATION_INC_CVR_VALIDATION_H_

typedef enum{
	CVR_VAL_PASS,
	CVR_VAL_FAIL
}cvr_validation_result_t;

#endif /* MODULES_VALIDATION_INC_CVR_VALIDATION_H_ */
