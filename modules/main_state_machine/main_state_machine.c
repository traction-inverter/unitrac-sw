/**
 * \file 		main_state_machine.c
 * \author 		Christian Vorobev
 * \date 		25 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "inc/main_state_machine.h"
#include "cmsis_os2.h"
#include "cvr_debug.h"

osThreadId_t main_state_machHandle;
void _init_function(void *);
cvr_sm_handle msm_sm;

msm_return_t msm_init(){
	msm_return_t _r = MSM_ERR_INIT;
	if(cvr_sm_new(msm_sm, _EVTCOUNT, _init_function, NULL, CVR_THREAD_PRIO_REALTIME) == CVR_SM_OK){
		_r = MSM_SUCCESS;
	}

	return _r;
}

void _init_function(void *args){
	/// TODO: implement transitions
}

msm_return_t msm_selftest(){
	msm_return_t _r = MSM_ERR_SELFTEST;

	/// Perform self-test routines for MSM here.
	_r = MSM_SUCCESS;

	return _r;
}
