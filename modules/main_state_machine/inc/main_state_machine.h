/**
 * \file 		main_state_machine.h
 * \author 		Christian Vorobev
 * \date 		26 Sep 2019
 * \warning 	This file is available under NDA: INSERT_NDA_HERE
 * \copyright 	(C) 2019, Christian Vorobev Private Research
 * \brief		INSERT_BRIEF_FILE_DESCRIPTION_HERE
 *
 * INSERT_DETAILED_FILE_DESCRIPTION_HERE
 *
 * Copyright (C) 2019, Christian Vorobev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef MODULES_MAIN_STATE_MACHINE_INC_MAIN_STATE_MACHINE_H_
#define MODULES_MAIN_STATE_MACHINE_INC_MAIN_STATE_MACHINE_H_

typedef int msm_event_id_t;

typedef struct msm_state* msm_state_handle;

typedef struct {
	msm_event_id_t event_id;
	void *args;
}msm_event_t;

typedef enum {

}msm_event_id_t;
#define _EVTCOUNT 10

typedef enum {
	MSM_SUCCESS,
	MSM_ERR_EVT_PUT,
	MSM_ERR_START,
	MSM_ERR_INIT,
	MSM_ERR_SELFTEST,
}msm_return_t;

msm_return_t msm_init();
msm_return_t msm_selftest();
msm_return_t msm_start();


#endif /* MODULES_MAIN_STATE_MACHINE_INC_MAIN_STATE_MACHINE_H_ */
