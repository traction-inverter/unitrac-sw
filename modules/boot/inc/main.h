/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "../../../system/STM32G4xx_HAL_Driver/inc/stm32g4xx_hal.h"
#include "../../../system/STM32G4xx_HAL_Driver/inc/stm32g4xx_ll_pwr.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define APA_IN_Pin GPIO_PIN_0
#define APA_IN_GPIO_Port GPIOC
#define APB_IN_Pin GPIO_PIN_1
#define APB_IN_GPIO_Port GPIOC
#define TRAC_FAULT_IN_Pin GPIO_PIN_0
#define TRAC_FAULT_IN_GPIO_Port GPIOA
#define TRAC_EN_OUT_Pin GPIO_PIN_1
#define TRAC_EN_OUT_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define USART1_TX_Pin GPIO_PIN_4
#define USART1_TX_GPIO_Port GPIOC
#define USART1_RX_Pin GPIO_PIN_5
#define USART1_RX_GPIO_Port GPIOC
#define ACHOP_IN_Pin GPIO_PIN_12
#define ACHOP_IN_GPIO_Port GPIOB
#define ACHOP_IN_EXTI_IRQn EXTI15_10_IRQn
#define ATOT_IN_Pin GPIO_PIN_13
#define ATOT_IN_GPIO_Port GPIOB
#define TRAC_CHOP_OUT_Pin GPIO_PIN_6
#define TRAC_CHOP_OUT_GPIO_Port GPIOC
#define CHO_Pin GPIO_PIN_8
#define CHO_GPIO_Port GPIOC
#define CLO_Pin GPIO_PIN_9
#define CLO_GPIO_Port GPIOC
#define AHO_Pin GPIO_PIN_8
#define AHO_GPIO_Port GPIOA
#define ALO_Pin GPIO_PIN_9
#define ALO_GPIO_Port GPIOA
#define BHO_Pin GPIO_PIN_10
#define BHO_GPIO_Port GPIOA
#define BLO_Pin GPIO_PIN_11
#define BLO_GPIO_Port GPIOA
#define T_SWDIO_Pin GPIO_PIN_13
#define T_SWDIO_GPIO_Port GPIOA
#define T_SWCLK_Pin GPIO_PIN_14
#define T_SWCLK_GPIO_Port GPIOA
#define T_SWO_Pin GPIO_PIN_3
#define T_SWO_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
